﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using Newtonsoft.Json;
using Cardio_core.DTO;
using Microsoft.AspNetCore.SignalR.Client;
using System.IO.Ports;
using System.Diagnostics;

namespace ConsoleApp2
{
    class Program
    {
        private static Random _random = new Random();
        static SerialPort SerialPort;

        static HubConnection Connection;

        static void Main(string[] args)
        {
            //Random rnd = new Random();
            //GetSerial();

            CreateConnection();

            StartConnection();

            Console.WriteLine("press a key to stop");
            Console.ReadKey();
        }

        private static void GetSerial()
        {
            List<string> test = new List<string>();
            SerialPort = new SerialPort();
            SerialPort.PortName = "COM4";
            SerialPort.BaudRate = 9600;
            SerialPort.Open();
        }

        private static async void StartConnection()
        {
            await Connection.StartAsync();
            while (true)
            {
                //ReadAndSend();
                ReadAndSendaRandom();
                Thread.Sleep(1000);
            }

        }




        private static void ReadAndSend()
        {
            string a = SerialPort.ReadExisting();
            int.TryParse(a, out int pulsation2);
            //int rd = rnd.Next(60, 80);
            
            try
            {
                if (pulsation2 > 0)
                {
                    Entry e2 = new Entry
                    {
                        HeartRate = pulsation2,
                        Time = DateTime.Now
                    };
                    //string result2 = JsonConvert.SerializeObject(e2);
                    var r = Connection.InvokeAsync("SendEntry", e2);
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            Console.WriteLine(pulsation2);
            //Console.WriteLine(rd);
            //Thread.Sleep(1000);
        }

        private static void CreateConnection()
        {
            Connection = new HubConnectionBuilder()
                    //.WithUrl("http://localhost/api_heartreate/entryhub/")
                    .WithUrl("http://serenotb.azurewebsites.net/entryhub/")
                    //.WithUrl("http://10.10.6.213/entryhub/")
                    .Build();
        }

        private static int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }

        private static void ReadAndSendaRandom()
        {
            
            int rd = RandomNumber(60, 80);

            try
            {
                if (rd > 0)
                {
                    Entry e2 = new Entry
                    {
                        HeartRate = rd,
                        Time = DateTime.Now
                    };
                    var r = Connection.InvokeAsync("SendEntry", e2);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            Console.WriteLine(rd);
        }
    }
}
